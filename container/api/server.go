package api

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/hexaforce/gin-air-service/gin-container/mq"
)

func SetupAPI(engine *gin.Engine, session *mq.Session) {

	engine.GET("/echo", Echo)

	engine.GET("/", Home)

	go func() {

		rand.New(rand.NewSource(time.Now().UnixNano()))

		_, queues, _ := session.GetDeclare()

		index := 0

		// Attempt to push a message every 2 seconds

		for {

			index++

			r := rand.Intn(9)

			message := []byte(fmt.Sprintf("[%d] %s_%d", r, os.Getenv("NAME"), index))

			time.Sleep(time.Millisecond * 500)
			// time.Sleep(time.Second * 1)

			if err := session.Push(message, queues[0].Queue, uint8(r)); err != nil {
				fmt.Printf("Push failed: %s\n", err)
			} else {
				fmt.Printf("Push succeeded! %s\n", string(message))
			}
		}

	}()

}
