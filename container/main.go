package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"

	"gitlab.com/hexaforce/gin-air-service/gin-container/api"
	"gitlab.com/hexaforce/gin-air-service/gin-container/mq"
)

var (
	addr = flag.String("addr", ":1321", "http service address")
	uri  = flag.String("uri", "amqp://guest:guest@rabbitmq:5672/", "AMQP URI")
)

func main() {

	flag.Parse()

	log.SetFlags(0)

	engine := gin.Default()

	engine.GET("/gin/health-check", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})

	session := mq.New(*uri)

	WORKER := strings.ToLower(os.Getenv("WORKER")) == "true"

	if WORKER {
		// time.Sleep(time.Second * 5)
		session.StreamStart()

	} else {

		api.SetupAPI(engine, session)

	}

	log.Fatal(engine.Run(*addr))

}
