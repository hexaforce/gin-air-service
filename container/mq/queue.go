package mq

import (
	"github.com/streadway/amqp"
)

type Exchange struct {
	Exchange   string
	Type       string
	Durable    bool
	AutoDelete bool
	Internal   bool
	NoWait     bool
	Arguments  amqp.Table
}

type Queue struct {
	Queue      string
	Durable    bool
	Exclusive  bool
	AutoDelete bool
	NoWait     bool
	Arguments  amqp.Table
}

type QueueBind struct {
	Queue      string
	Exchange   string
	RoutingKey string
	NoWait     bool
	Arguments  amqp.Table
}

func (session *Session) GetDeclare() (exchanges []Exchange, queues []Queue, queue_binds []QueueBind) {
	// Exchange --------------------------
	exchanges = []Exchange{}
	// exchanges = append(exchanges,
	//  Exchange{
	//   Exchange:   "line.default.direct",
	//   Type:       "direct",
	//   Durable:    true,
	//   AutoDelete: false,
	//   Internal:   false,
	//   NoWait:     false,
	//   Arguments:  amqp.Table{},
	//  })

	// Queue --------------------------
	queues = []Queue{}
	queues = append(queues,
		Queue{
			Queue:      "line.sent",
			Durable:    true,
			Exclusive:  false,
			AutoDelete: false,
			NoWait:     false,
			Arguments:  nil,
		})
	queues = append(queues,
		Queue{
			Queue:      "line.sent.error",
			Durable:    true,
			Exclusive:  false,
			AutoDelete: false,
			NoWait:     false,
			Arguments: amqp.Table{
				"x-dead-letter-exchange":    "",
				"x-dead-letter-routing-key": "line.sent",
			},
		})

	// QueueBind --------------------------
	queue_binds = []QueueBind{}
	// queue_binds = append(queue_binds,
	//  QueueBind{
	//   Queue:      "line.sent.text",
	//   Exchange:   "",
	//   RoutingKey: "line.sent.text.*",
	//   NoWait:     false,
	//   Arguments:  amqp.Table{},
	//  })

	return
}
