package mq

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/streadway/amqp"
)

// Stream will continuously put queue items on the channel.
// It is required to call delivery.Ack when it has been successfully processed, or delivery.Nack when it fails.
// Ignoring this will cause data to build up on the server.
func (session *Session) Stream(QueueName string) (<-chan amqp.Delivery, error) {
	if !session.isReady {
		return nil, errNotConnected
	}
	return session.channel.Consume(
		QueueName,
		"",    // Consumer
		false, // Auto-Ack
		false, // Exclusive
		false, // No-local
		false, // No-Wait
		nil,   // Args
	)
}

// StreamStart will continuously put queue items on the channel.
func (session *Session) StreamStart() {

	_, queues, _ := session.GetDeclare()
	deliveries, err := session.Stream(queues[0].Queue)

	if err != nil {
		fmt.Printf("Push failed: %s\n", err)
	}

	rand.New(rand.NewSource(time.Now().UnixNano()))

	go func() {
		for delivery := range deliveries {

			// takes 50 to 60 milliseconds to process.
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(50)+300))
			r := rand.Intn(5)
			// if r == 2 || r == 3 || r == 4 { // 60%
			// if r == 3 || r == 4 { // 40%
			if r == 4 { // 20%
				retryCount := delivery.Headers["x-retry-count"].(int32)
				if retryCount > 1 {

					fmt.Printf("\x1b[31m<<<< Stream error! %s \x1b[0m \n", delivery.Body)
					delivery.Ack(false)

				} else {

					retryCount++
					// TTL := int(factorial(int(retryCount + 1)))
					session.PushRetryWithTTL(delivery.Body, "", queues[1].Queue, retryCount, uint8(r), 3000)

					fmt.Printf("\x1b[33m<<<< Stream warn! %s \x1b[0m \n", delivery.Body)
					delivery.Ack(false)
					// delivery.Nack(false, false)
				}

			} else {
				delivery.Ack(false)
				fmt.Printf("\x1b[32mStream success! %s \x1b[0m \n", delivery.Body)
			}

		}
	}()
}
