package mq

import (
	"errors"
	"fmt"
	"time"

	"github.com/streadway/amqp"
)

// Push will push data onto the queue, and wait for a confirm.
// If no confirms are received until within the resendTimeout, it continuously re-sends messages until a confirm is received.
// This will block until the server sends a confirm. Errors are only returned if the push action itself fails, see UnsafePush.
func (session *Session) Push(data []byte, QueueName string, Priority uint8) error {
	return session.PushRetryWithTTL(data, "", QueueName, 0, Priority, 0)
}

func (session *Session) PushRetryWithTTL(data []byte, Exchange string, QueueName string, RetryCount int32, Priority uint8, Expiration int) error {
	header := make(amqp.Table)
	header["x-retry-count"] = RetryCount
	return session.PushWithHeader(data, Exchange, QueueName, Priority, Expiration, header)
}

func (session *Session) PushWithHeader(data []byte, Exchange string, RoutingKey string, Priority uint8, Expiration int, header amqp.Table) error {
	if !session.isReady {
		return errors.New("failed to push: not connected")
	}
	for {
		err := session.UnsafePush(data, Exchange, RoutingKey, Priority, Expiration, header)
		if err != nil {
			session.logger.Println("Push failed. Retrying...")
			select {
			case <-session.done:
				return errShutdown
			case <-time.After(resendDelay):
			}
			continue
		}
		select {
		case confirm := <-session.notifyConfirm:
			if confirm.Ack {
				// session.logger.Println("Push confirmed!")
				return nil
			}
		case <-time.After(resendDelay):
		}
		session.logger.Println("Push didn't confirm. Retrying...")
	}
}

// UnsafePush will push to the queue without checking for confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will recieve the message.
func (session *Session) UnsafePush(data []byte, Exchange string, RoutingKey string, Priority uint8, Expiration int, header amqp.Table) error {
	if !session.isReady {
		return errNotConnected
	}
	return session.channel.Publish(
		Exchange,   // Exchange
		RoutingKey, // Routing key
		false,      // Mandatory
		false,      // Immediate
		amqp.Publishing{
			Headers:      header,
			ContentType:  "text/plain",
			Body:         data,
			DeliveryMode: amqp.Persistent,               // 1=non-persistent, 2=persistent
			Priority:     Priority,                      // 0-9
			Expiration:   fmt.Sprintf("%d", Expiration), // milliseconds
		},
	)
}
