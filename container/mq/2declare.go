package mq

import (
	"log"

	"github.com/streadway/amqp"
)

// init will initialize channel & declare queue
func (session *Session) init(conn *amqp.Connection) error {

	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	// ch.Qos()

	err = ch.Confirm(false)
	if err != nil {
		return err
	}

	exchanges, queues, queue_binds := session.GetDeclare()

	for _, exchange := range exchanges {
		err = ch.ExchangeDeclare(
			exchange.Exchange,
			exchange.Type,
			exchange.Durable,
			exchange.AutoDelete,
			exchange.Internal,
			exchange.NoWait,
			exchange.Arguments,
		)
		if err != nil {
			return err
		}
	}

	for _, queue := range queues {
		_, err = ch.QueueDeclare(
			queue.Queue,
			queue.Durable,
			queue.AutoDelete,
			queue.Exclusive,
			queue.NoWait,
			queue.Arguments,
		)
		if err != nil {
			return err
		}
	}

	for _, queue_bind := range queue_binds {
		err = ch.QueueBind(
			queue_bind.Queue,
			queue_bind.RoutingKey,
			queue_bind.Exchange,
			queue_bind.NoWait,
			queue_bind.Arguments,
		)
		if err != nil {
			return err
		}
	}

	session.changeChannel(ch)
	session.isReady = true
	log.Println("Setup!")

	return nil
}
